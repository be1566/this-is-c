// practice01.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
#include <string>

int main()
{
	std::string strName;
	std::cout << "이름: ";
	std::cin >> strName;

	int nAge;
	std::cout << "나이: ";
	std::cin >> nAge;

	std::cout << "나의 이름은 " << strName << "이고, " << nAge << "살입니다." << std::endl;

    return 0;
}

