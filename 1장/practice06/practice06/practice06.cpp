// practice06.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

void Swap(int &a, int &b)
{
	int nTmp = a;
	a = b;
	b = nTmp;
}

int main()
{
	int aList[5] = { 10, 20, 30, 40, 50 };
	int nMin;

	for (int i = 0; i < 4; ++i)
	{
		nMin = i;
		for (int k = i + 1; k < 5; ++k)
			if (aList[k] < aList[nMin])
				nMin = k;
		Swap(aList[i], aList[nMin]);
	}

	for (auto nData : aList)
		cout << nData << ' ';

	cout << endl;

    return 0;
}

