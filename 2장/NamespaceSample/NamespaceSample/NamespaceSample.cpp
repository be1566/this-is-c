// NamespaceSample.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>

namespace TEST
{
	int g_nData = 100;

	void TestFunc(void)
	{
		std::cout << "TEST::TestFunc()" << std::endl;
	}
}

int main()
{
	TEST::TestFunc();
	std::cout << TEST::g_nData << std::endl;
    return 0;
}

