// FuncTemplate1.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>

template <typename T>
T TestFunc(T a)
{
	std::cout << "매개변수 a: " << a << std::endl;

	return a;
}

int main()
{
	std::cout << "int\t" << TestFunc(3) << std::endl;
	std::cout << "double\t" << TestFunc(3.3) << std::endl;
	std::cout << "char\t" << TestFunc('A') << std::endl;
	std::cout << "char*\t" << TestFunc("TestString") << std::endl;

    return 0;
}

