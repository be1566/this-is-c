// FuncTemplate2.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>

template <typename T>
T Add(T a, T b)
{
	return a + b;
}

int main()
{
	std::cout << Add(3, 4) << std::endl;
	std::cout << Add(3.3, 4.4) << std::endl;

    return 0;
}

