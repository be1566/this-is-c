// lacturepractice01.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>

int main()
{
	// 포인터의 문제점! 변수인것이 문제다!
	int nData = 10;
	int nNewData = 20;

	int *pnData = &nData;
	// nData = 5;
	*pnData = 5;

	pnData = &nNewData;
	// nNewData = 5;
	*pnData = 5;
    return 0;
}

