// lacturepractice02.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

class CTest
{
public:
	CTest()
	{
		m_nData = 0;
	}

	~CTest()
	{

	}

	void PrintData()
	{
		int local = 5;
		cout << this << endl;	// Break Point this로 확인
		cout << m_nData << endl;
		cout << this->m_nData << endl;
	}
	
protected:
	int m_nData = 0;	// C++11
};

int main()
{
	CTest a;
	cout << &a << endl;
	a.PrintData();				// Break Point &a로 확인

	CTest b;
	cout << &b << endl;
	b.PrintData();				// Break Point &b로 확인

    return 0;
}

