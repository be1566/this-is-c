// MemberNotOver.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

void TestFunc(int nParam)
{
	cout << nParam << endl;
}

int main()
{
	TestFunc(10);
	TestFunc(5.5);

    return 0;
}

