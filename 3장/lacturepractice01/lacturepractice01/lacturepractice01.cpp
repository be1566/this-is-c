// lacturepractice01.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>

class USERDATA
{
public:
	int nAge;
	char szName[32];

	void Print(USERDATA *pUser)
	{
		printf("%d, %s\n", pUser->nAge, pUser->szName);
	}

	void Print2()
	{
		printf("%d, %s\n", nAge, szName);
	}
};

int main()
{
	USERDATA a = { 20, "Hong" };
	a.Print(&a);	// 내부적으로는 이와 같은 코드가 실행됨
	a.Print2();		// 실제 작성시는 이와 같은 코드가 작성 빈 매개변수에는 앞 객체의 주소가 들어간다고 생각하면 된다!
    return 0;
}

