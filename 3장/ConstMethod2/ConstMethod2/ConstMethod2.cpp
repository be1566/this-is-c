// ConstMethod2.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

class CTest
{
public:
	CTest(int nParam) : m_nData(nParam) { };
	~CTest() { }

	// 상수형 메서드로 선언 및 정의했다.
	int GetData() const
	{
		// 상수형 메서드라도 mutable 멤버 변수에는 값을 쓸 수 있다.
		m_nData = 20;
		return m_nData;
	}

	int SetData(int nParam) { m_nData = nParam; }

private:
	mutable int m_nData = 0;
};

int main()
{
	CTest a(10);
	cout << a.GetData() << endl;

    return 0;
}

