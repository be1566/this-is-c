// ExpDefaultConstructor.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

class CTest
{
public:
	// 디폴트 생성자 선언 및 정의!
	CTest(void) = default;
	
	// 디폴트 생성자 선언
	CTest(void);
	int m_nData = 5;
};

// 클래스 외부에서 디폴트 생성자 정의
CTest::CTest(void) { }

int main()
{
	CTest a;
	cout << a.m_nData << endl;

    return 0;
}

